// Nav ja starts
var  mn = $(".main-nav");
    mns = "main-nav-scrolled";
    hdr = $('header').height();

$(window).scroll(function() {
  if( $(this).scrollTop() > hdr ) {
    mn.addClass(mns);
  } else {
    mn.removeClass(mns);
  }
});
// Nav js ends

//Filter Menu 
        if ($.fn.isotope) {
            $('.our_menu_tabs li').on("click", function () {
                $(".our_menu_tabs li").removeClass("active");
                $(this).addClass("active");
                var selector = $(this).attr('data-name');
                $(".isotope").isotope({
                    filter: selector,
                    animationOptions: {
                        duration: 750,
                        easing: 'linear',
                        queue: false
                    }
                });
                return false;
            });
};

// Tooltips Initialization
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

// Owl carosuel

$(document).ready(function(){
  $('.owl-carousel').owlCarousel();
});

var owl = $('.owl-carousel');
owl.owlCarousel({
    items:1,
    rtl:true,
    animateOut: 'slideOutDown',
    animateIn: 'flipInX',
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true
});
        
